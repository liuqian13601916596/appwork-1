package com.work.appwork;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
//@ComponentScan("com.work.appwork.adminController")
@MapperScan(basePackages = {"com.work.appwork.dao"})
@EnableTransactionManagement
public class AppworkApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppworkApplication.class, args);
    }

}
