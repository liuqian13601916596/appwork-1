package com.work.appwork.MyInterFace;


//@interface 自定义注解
public @interface MyiMua {
    public int id() default -1;

    public String msg() default "Hi";
}
