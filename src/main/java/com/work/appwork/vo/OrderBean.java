package com.work.appwork.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.work.appwork.JsonSerializer.CustomDateSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Date;

@Data
@Service
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OrderBean implements Serializable {
    private int id;
    private String orderName;

    //在需要的实体类get方法上加上  @JsonSerialize  using参数指定class
    @JsonSerialize(using = CustomDateSerialize.class)
    private Date createDate;

}
