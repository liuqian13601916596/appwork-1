package com.work.appwork.vo;

import com.sun.xml.internal.ws.developer.Serialization;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.stereotype.Service;

import java.io.Serializable;

@Service(value = "AdminVo")
@Data
//@Getter
//@Setter
@NoArgsConstructor
@AllArgsConstructor //会添加一个构造函数 涵盖了所有已声明字段属性参数
//@Builder(toBuilder = true)
@Builder
/**
 * @Builder注解修改原对象的属性值
 *修改实体，要求实体上添加@Builder(toBuilder=true)
 */
//@Serialization
public class AdminVo implements Serializable {
    private  int id;
    @ApiModelProperty(value = "用户名",example = "我爱祖国",required = true)

    private String adminName;
    private String adminPwd;


}
