package com.work.appwork.util;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * redisson配置类
 */
@Configuration
public class RedissonConfig {
    @Value("${spring.redis.host}")
    private String host;
    @Value("${spring.redis.port}")
    private String port;
    //redis 有密码时在输入
//    @Value("${spring.redis.password}")
//    private String password;

    @Value("${spring.redis.database}")
    private int database;

    @Bean
    public RedissonClient getClient() {
        String address = "redis://" + host + ":" + port;
        Config config = new Config();
        /**单机模式*/
        config.useSingleServer().setAddress(address).setDatabase(database);


        /**哨兵模式*/
//              config.useSentinelServers()
//                    .addSentinelAddress(address)
//                    .setMasterName("mymaster")
//                    .setPassword(password)
//                    .setDatabase(database);
        RedissonClient redisson = Redisson.create(config);
        return redisson;
    }
}
