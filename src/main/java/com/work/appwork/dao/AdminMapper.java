package com.work.appwork.dao;


import com.work.appwork.request.LoginRequest;
import com.work.appwork.vo.AdminVo;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Transactional  //数据库连接池 （声明式事务管理）
public interface AdminMapper {
    public AdminVo getAdminById(String adminId);

    /**
     * 请求参数 用户名和密码
     * @param loginRequest
     * @return
     */
    public AdminVo login(LoginRequest loginRequest);
    public List<AdminVo> findAll();
    //添加admin
    public int saveAdmin(AdminVo adminVo);
    public int adminDelete(int  id);//删除admin用户
    public AdminVo searchAdmin(int id);//根据id查询用户
    public int editAdmin(AdminVo adminVo);//修改用户
}
