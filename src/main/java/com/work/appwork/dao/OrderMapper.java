package com.work.appwork.dao;

import com.work.appwork.vo.OrderBean;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface OrderMapper {
    public int insertOrder(OrderBean orderBean);//插入一条订单信息
    public OrderBean findOrderById(int id);//通过ID查询订单
    public int deleteOrderById(int id);

}
