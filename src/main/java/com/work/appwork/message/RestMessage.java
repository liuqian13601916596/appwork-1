package com.work.appwork.message;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@ApiModel(value = "Rest响应",description = "返回响应数据")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Configuration
public class RestMessage {

    @ApiModelProperty(value = "是否成功")
    private boolean isSuccess=true;
    @ApiModelProperty(value = "返回对象")
    private Object data;
    @ApiModelProperty(value = "错误信息")
    private String message;
}
