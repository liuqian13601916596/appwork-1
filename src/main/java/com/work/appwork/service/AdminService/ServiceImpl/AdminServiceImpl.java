package com.work.appwork.service.AdminService.ServiceImpl;

import com.work.appwork.dao.AdminMapper;
import com.work.appwork.request.LoginRequest;
import com.work.appwork.service.AdminService.AdminService;
import com.work.appwork.vo.AdminVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class AdminServiceImpl implements AdminService {
    @Resource
    private AdminMapper adminMapper;


    @Override
    public AdminVo getAdminVo(String id) {
        return adminMapper.getAdminById(id);
    }

    @Override
    public AdminVo login(LoginRequest loginRequest) {
        return adminMapper.login(loginRequest);
    }

    @Override
    public List<AdminVo> findAll() {
        return adminMapper.findAll();
    }

    @Override
    public int saveAdmin(AdminVo adminVo) {
        return adminMapper.saveAdmin(adminVo);
    }

    @Override
    public int adminDelete(int  id) {
        return adminMapper.adminDelete(id);
    }

    @Override
    public AdminVo searchAdmin(int id) {
        return adminMapper.searchAdmin(id);
    }

    @Override
    public int editAdmin(AdminVo adminVo) {
        return adminMapper.editAdmin(adminVo);
    }
}
