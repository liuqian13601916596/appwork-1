package com.work.appwork.service.AdminService;

import com.work.appwork.request.LoginRequest;
import com.work.appwork.vo.AdminVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


public interface AdminService {
    AdminVo getAdminVo(String id);
    public AdminVo login(LoginRequest loginRequest);
    public List<AdminVo> findAll();
    //添加admin
    public int saveAdmin(AdminVo adminVo);
    public int adminDelete(int  id);//删除admin用户
    public AdminVo searchAdmin(int id);//根据id查询用户
    public int editAdmin(AdminVo adminVo);//修改用户
}
