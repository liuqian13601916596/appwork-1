package com.work.appwork.service.OrderService;


import com.work.appwork.vo.OrderBean;

public interface OrderService {
    public int insertOrder(OrderBean orderBean);
    public OrderBean findOrderById(int id);//通过ID查询订单
    public int deleteOrderById(int id); //通过ID删除订单
}
