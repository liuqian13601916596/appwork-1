package com.work.appwork.service.OrderService.ServiceImpl;

import com.work.appwork.dao.OrderMapper;
import com.work.appwork.service.OrderService.OrderService;
import com.work.appwork.vo.OrderBean;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class OrderSerivceImpl implements OrderService {

    @Resource
    private OrderMapper orderMapper;

    @Override
    public int insertOrder(OrderBean orderBean) {
        return orderMapper.insertOrder(orderBean);
    }

    @Override
    public OrderBean findOrderById(int id) {
        return orderMapper.findOrderById(id);
    }

    @Override
    public int deleteOrderById(int id) {
        return orderMapper.deleteOrderById(id);
    }
}
