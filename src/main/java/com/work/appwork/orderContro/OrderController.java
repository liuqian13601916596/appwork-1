package com.work.appwork.orderContro;

import com.alibaba.fastjson.JSON;
import com.work.appwork.message.RestMessage;
import com.work.appwork.service.OrderService.OrderService;
import com.work.appwork.vo.OrderBean;
import io.swagger.annotations.*;
import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;

@RestController
@RequestMapping(value = {"/order"})
@Api(value = "orderController",tags = {"order订单操作接口"})//接口文档说明
@Slf4j //作日志文件输出

public class OrderController {

    @Resource
    private OrderService orderService;

    //redis缓存
    @Resource
    private RedisTemplate<String,String> redisTemplate;

    //redis分布式锁 单机模式
    @Resource
    private RedissonClient redissonClient;

    @ApiOperation(value = "添加order信息")
    @GetMapping(value = {"/insertOrder"})
//    @ApiImplicitParam(name = "createDate",value = "创建order时间",required = true)
    @ApiParam(value = "orderName",required = true)
    public RestMessage insertOrder(@RequestParam(value = "orderName") String orderName){
        System.out.println("这是dev环境");
        RestMessage restMessage=null;
        log.info("准备添加order:{}",orderName);
        OrderBean orderBean=OrderBean.builder()
                .orderName(orderName)
                .createDate(new Date())
                .build();

        System.out.println(" time= " + new Date());
        Integer result=orderService.insertOrder(orderBean);
        //返回消息对象
        if(result!=0){
             restMessage=new RestMessage(true,orderBean,"添加行数"+result);
        }else {
             restMessage=new RestMessage(false,orderBean,"添加行数"+result);
        }
        return restMessage;
    }

    @ApiOperation(value = "ID查询order信息")
    @GetMapping(value = {"/selectOrder"})
    @ApiParam(value = "id",required = true)
//    @ApiImplicitParam(name = "id",value = "ID",required = true)
    public OrderBean selectOrder(@RequestParam(value = "id") int id){
        OrderBean orderBean=null;
        StringBuffer stringBuffer=new StringBuffer();
        if(id!=0){
            orderBean=orderService.findOrderById(id);
            /********111212*/
            stringBuffer.append("order:");
            stringBuffer.append(id);
            stringBuffer.append(orderBean.getOrderName());
            stringBuffer.append(orderBean.getCreateDate());
            String key=stringBuffer.toString();
            log.info("缓存中查询Key：",key);
            String data=redisTemplate.opsForValue().get(key);
            if(StringUtils.isNotBlank(data)){
                //有json数据转换成 orderBean
                orderBean=JSON.parseObject(data,OrderBean.class);
                log.info("获取到order数据:{}",orderBean);
            }else {
                    log.info("从缓存中获取order信息:{}",id);
                orderBean=orderService.findOrderById(id);
                if(orderBean!=null){
                    log.info("从数据库中获取order信息:{}",id);
                    //缓存到redis中
                    redisTemplate.opsForValue().set(key,JSON.toJSONString(orderBean));
                }
            }
        }
        return orderBean;
    }

    @Resource
    private RestMessage restMessage;

    @ApiOperation(value = "ID删除order")
    @GetMapping(value = {"/deleteOrderByid"})
    @ApiParam(value = "id",required = true)
    /**
     * @ApiResponses、@ApiResponse：方法返回值的状态码说明
     * */
//    @ApiResponses({
//            @ApiResponse(code = 200,message = "请求成功"),
//            @ApiResponse(code = 400,message = "请求参数没填好"),
//            @ApiResponse(code = 404,message = "请求路径没有或页面跳转路径不对")
//    })
    public RestMessage deleteOrderByid(@RequestParam(value = "id")int id ){
        //判断id不为空 ，一般情况不为空
        StringBuffer stringBuffer=new StringBuffer();
        OrderBean orderBean=null;
        orderBean=orderService.findOrderById(id);
        if(orderBean!=null){
            stringBuffer.append("order:");
            stringBuffer.append(orderBean.getOrderName());
            stringBuffer.append(orderBean.getCreateDate());
            String key=stringBuffer.toString();
            log.info("删除缓存中的Key：",key);
            redisTemplate.delete(key);
            Integer result=orderService.deleteOrderById(id);
            //返回消息对象
            if(result!=0){
                restMessage=new RestMessage(true,orderBean,"删除行数"+result);
            }else {
                restMessage=new RestMessage(false,orderBean,"删除行数"+result);
            }
        }

        return restMessage;
    }



}
