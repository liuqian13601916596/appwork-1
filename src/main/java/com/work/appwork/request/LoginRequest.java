package com.work.appwork.request;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class LoginRequest implements Serializable {
    private String adminName;
    private String adminPwd;
}
